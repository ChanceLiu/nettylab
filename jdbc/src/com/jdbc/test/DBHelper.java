package com.jdbc.test;

/**
 * Created by ChanceLiu on 15/5/5.
 */

import java.sql.*;
import java.util.Properties;

public class DBHelper {
    public static final String url = "jdbc:mysql://localhost:8889/lezhang";
    public static final String name = "com.mysql.jdbc.Driver";
    public static final String user = "root";
    public static final String password = "root";
    public static final String characterEncoding = "UTF8";

    public Connection conn = null;
    public PreparedStatement pst = null;

    public DBHelper() {

        Properties properties = new Properties();
        properties.put("user", user);
        properties.put("password", password);
        properties.put("useUnicode", "true");
        properties.put("characterEncoding", characterEncoding);

        try {
            // 之所以要使用下面这条语句，是因为要使用MySQL的驱动，所以我们要把它驱动起来，
            // 可以通过Class.forName把它加载进去，也可以通过初始化来驱动起来，下面三种形式都可以

            Class.forName(name);//指定连接类型, 动态加载mysql驱动
            // or:
            //com.mysql.jdbc.Driver driver = new com.mysql.jdbc.Driver();
            // or：
            // new com.mysql.jdbc.Driver();

            System.out.println("成功加载MySQL驱动程序");

            conn = DriverManager.getConnection(url, properties);//获取连接
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void close() {
        try {
            this.conn.close();
            this.pst.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws Exception {

        DBHelper dbHelper = new DBHelper();
        String sql = "create table student(NO char(20),name varchar(20),primary key(NO))";

        try {


            // 一个Connection代表一个数据库连接
            // Statement里面带有很多方法，比如executeUpdate可以实现插入，更新和删除等
            Statement stmt = dbHelper.conn.createStatement();
            int result=-1;
//             result = stmt.executeUpdate(sql);// executeUpdate语句会返回一个受影响的行数，如果返回-1就没有成功
//            if (result != -1) {
                System.out.println("创建数据表成功");
                sql = "insert into student(NO,name) values('2012001','lucy')";
                result = stmt.executeUpdate(sql);
                sql = "insert into student(NO,name) values('2012002','lily')";
                result = stmt.executeUpdate(sql);
                sql = "select * from student";
                ResultSet rs = stmt.executeQuery(sql);// executeQuery会返回结果的集合，否则返回空值
                System.out.println("学号\t姓名");
                while (rs.next()) {
                    System.out
                            .println(rs.getString(1) + "\t" + rs.getString(2));// 入如果返回的是int类型可以用getInt()
                }
//            }
        } catch (SQLException e) {
            System.out.println("MySQL操作错误");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbHelper.conn.close();
        }
    }

}
