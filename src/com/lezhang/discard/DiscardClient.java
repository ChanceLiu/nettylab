/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.lezhang.discard;

import com.google.gson.Gson;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * Keeps sending random data to the specified address.
 */
public class DiscardClient {

    private final String host;
    private final int port;
    private final int firstMessageSize;
    private  String msg;

    public DiscardClient(String host, int port, int firstMessageSize) {
        this.host = host;
        this.port = port;

        Account account=new Account("luce","female","12");
        msg = new Gson().toJson(account);

        this.firstMessageSize = msg.length();
    }

    public void run() throws Exception {



        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(group)
             .channel(NioSocketChannel.class)
             .handler(new DiscardClientHandler(firstMessageSize));

            // Make the connection attempt.
            ChannelFuture f = b.connect(host, port).sync();



            Channel channel = f.channel();

            channel.writeAndFlush(msg);

            // Wait until the connection is closed.
            channel.closeFuture().sync();
        } finally {
            group.shutdownGracefully();
        }
    }

    public static void main(String[] args) throws Exception {
        // Print usage if no argument is specified.
//        if (args.length < 2 || args.length > 3) {
//            System.err.println(
//                    "Usage: " + DiscardClient.class.getSimpleName() +
//                    " <host> <port> [<first message size>]");
//            return;
//



        // Parse options.
        final String host = "localhost";
//        final String host = args[0];
        final int port = 8080;
//        final int port = Integer.parseInt(args[1]);
        final int firstMessageSize;
        if (args.length == 3) {
            firstMessageSize = Integer.parseInt(args[2]);
        } else {
            firstMessageSize = 256;
        }
        new DiscardClient(host, port, firstMessageSize).run();
    }
}
